<?php

use Illuminate\Database\Seeder;
use App\DiscountCode; 
class voucher_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DiscountCode::create([
            'discount_code' => 'aabc123', 
            'percentage' => 5
        ]); 

        DiscountCode::create([
            'discount_code' => '11c123', 
            'percentage' => 3
        ]); 

        
    }
}
