<?php

use Illuminate\Database\Seeder;
use App\Product; 
class product_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Product::create([
            'name' => 'Shampoo',
            'image' => 'https://images-na.ssl-images-amazon.com/images/I/61ziQXS5SbL._SL1080_.jpg', 
            'price'=> 5000
        ]); 

        Product::create([
            'name' => 'Soap',
            'image' => 'https://images-na.ssl-images-amazon.com/images/I/61OZ1aProFL._SX466_.jpg', 
            'price'=> 2000
        ]); 

        Product::create([
            'name' => 'Guitar',
            'image' => 'https://cdn.shopify.com/s/files/1/0117/8740/3323/products/T25-TS-BTE_1542361171550_1200x1200.jpg?v=1553147983', 
            'price'=> 20000
        ]); 
    }
}
