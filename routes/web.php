<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'cart'], function () {
    Route::get('/add-to-cart/{product_id}/{amount} ', 'CartController@addToCart')->name('cart.add-to-cart'); 
    Route::get('/qty-change/{product_id}/{amount} ', 'CartController@changeQty')->name('cart.change-qty');
    Route::get('/remove/{product_id}', 'CartController@removeProduct')->name('cart.remove-product'); 
    Route::get('/disc/{product_id}/{voucher_id}','CartController@discount')->name('cart.add-disc'); 
});
