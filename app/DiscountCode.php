<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountCode extends Model
{
    //
    protected $table ='discount_code';
    protected $fillable = [
        'discount_code', 'percentage'];
}
