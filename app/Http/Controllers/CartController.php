<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Product; 
use DiscountCode; 
use Cart; 
use Illuminate\Support\Facades\DB;
class CartController extends Controller
{
    //

    public function index() { 
        $query = DB::table('cart')
                ->join('products', 'cart.product_id', '=', 'products.id')
                ->select('products.name', 'cart.qty', 'cart.price', 'cart.discount'); 
        
        
    }

    public function addToCart($product_id,$amount) { 
        $product = Product::find($product_id); 
         Cart::create([
             'product_id' => $product->id, 
             'qty' => $amount, 
             'price' => $amount * $product_id->price, 
             'discount' => 0 
         ]);
    }

    public function changeQty($cart_id,$amount) { 
      $cart = Cart::find($cart_id); 

      $product = Product::find($cart->id); 

      $cart->amount = $amount;
      $cart->price = $amount * $product->price; 
      $cart->save() ;
    }


    public function removeProduct($cart_id) { 
        $cart = Cart::find($cart_id); 
        $cart->delete(); 

      }

}
